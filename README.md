## 说明

### 环境

建议使用conda安装环境，可以很方便的安装cuda和cudnn。如有必要也可以直接安装在系统python环境中或使用docker环境。

#### conda

- 安装conda

安装conda需要下载anaconda，可于[清华大学开源软件镜像站](https://mirror.tuna.tsinghua.edu.cn/help/anaconda/) 根据页面指引**下载安装anaconda3**并**配置国内源**。(最好安装最新版本）

linux系统下需要根据安装结束时的提示修改系统变量，应该是在要~/.bashrc文件中添加一行再```source ~/.bashrc```，具体以提示为准。

- 创建新的虚拟环境

```bash
conda create -n tf1 python==3.6
```

- 切换到该环境

```bash
conda activate tf1
```

- 安装所需环境

如果使用GPU，(30系列的显卡可以安装但可能不能正常调用cuda)

```bash
conda install pytorch==1.0.1 torchvision==0.2.2 cudatoolkit=9.0 -c pytorch
```

如果使用CPU

```bash
conda install pytorch-cpu==1.0.1 torchvision-cpu==0.2.2 cpuonly -c pytorch
```

安装其他包

```bash
conda install Click==7.0 numpy==1.16.3 yacs==0.1.6 matplotlib tqdm 
pip install tensorboardX==1.8 opencv-python 
```

如果运行时提示错误，可根据报错进行安装 pip/conda instlal **

#### pip

如果不使用虚拟环境：

- 安装python3
  一般系统都会自带python的，确认版本为3以上，亲测3.6可用，其余版本未验证
- 安装显卡驱动/CUDA/CUDNN （如果需要用到GPU）
  可自行查找方法，cuda需要9.0 cudnn==7.6(同上，30系列的显卡可以安装但可能不能正常调用cuda)

- 安装依赖
  切换到工作目录

```bash
pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple
```

如果pytorch安装失败，在requirement.txt文件中删掉那两行，先重新运行上述命令安装其他包。
然后在[torch download](https://download.pytorch.org/whl/cu90/torch_stable.html)这里下载1.0.1版本的whl文件本地安装，下载时需确保下载的版本与python/cuda(cpu)/操作系统 的版本相吻合。下载完成后再次pip install即可。

#### docker

如果有需要也可以使用docker配置所需环境，可以拉取pytorch的镜像作为基本再安装其他包。此处暂不详述。
如需使用gpu则需要安装nvidia-docker。

### 运行

--cfg参数指定数据集，会自动下载，
CPU_MODE指定为True时使用CPU，默认使用GPU。
main/train.py用以训练，main/valid.py用以测试。

```bash
# To train long-tailed CIFAR-10 with imbalanced ratio of 50:
python main/train.py  --cfg configs/cifar10.yaml   

# To validate with the best model:
python main/valid.py  --cfg configs/cifar10.yaml

# To debug with CPU mode:
python main/train.py  --cfg configs/cifar10.yaml   CPU_MODE True
```

已保存一个最佳存档，可以直接用于测试。

### tensorboard

可以使用tensorboard查看训练详情。在工作根目录运行以下命令，或者根据所在路径调整其中的logdir

```bash
tensorboard --logdir='./output/cifar10/BBN.CIFAR10.res32.200epoch/tensorboard --host=127.0.0.1'
```

在浏览器中打开127.0.0.1:6006即可查看训练过程中loss和acc的变化。
![tensorboard](./output/pic.png)

=========================end========================


## BBN: Bilateral-Branch Network with Cumulative Learning for Long-Tailed Visual Recognition
Boyan Zhou, Quan Cui, Xiu-Shen Wei*, Zhao-Min Chen

This repository is the official PyTorch implementation of paper [BBN: Bilateral-Branch Network with Cumulative Learning for Long-Tailed Visual Recognition](https://arxiv.org/abs/1912.02413). (The work has been accepted by [CVPR2020](http://cvpr2020.thecvf.com/), **Oral Presentation**)

## Main requirements

  * **torch == 1.0.1**
  * **torchvision == 0.2.2_post3**
  * **tensorboardX == 1.8**
  * **Python 3**

## Environmental settings
This repository is developed using python **3.5.2/3.6.7** on Ubuntu **16.04.5 LTS**. The CUDA nad CUDNN version is **9.0** and **7.1.3** respectively. For Cifar experiments, we use **one NVIDIA 1080ti GPU card** for training and testing. (**four cards for iNaturalist ones**). Other platforms or GPU cards are not fully tested.

## Pretrain models for iNaturalist

We provide the BBN pretrain models of both 1x scheduler and 2x scheduler for iNaturalist 2018 and iNaturalist 2017.

iNaturalist 2018: [Baidu Cloud](https://pan.baidu.com/s/1olDppTptZ5HYWsgQsMCPLQ), [Google Drive](https://drive.google.com/open?id=1B9ZEfMHqE-KQRKX6nQLQRm8ErFrnHaoE)

iNaturalist 2017: [Baidu Cloud](https://pan.baidu.com/s/1soxsHKKblhapew_wuEdKPQ), [Google Drive](https://drive.google.com/open?id=1yHme1iFQy-Lz_11yZJPlNd9bO_YPKlEU)

## Usage
```bash
# To train long-tailed CIFAR-10 with imbalanced ratio of 50:
python main/train.py  --cfg configs/cifar10.yaml     

# To validate with the best model:
python main/valid.py  --cfg configs/cifar10.yaml

# To debug with CPU mode:
python main/train.py  --cfg configs/cifar10.yaml   CPU_MODE True
```

You can change the experimental setting by simply modifying the parameter in the yaml file.

## Data format

The annotation of a dataset is a dict consisting of two field: `annotations` and `num_classes`.
The field `annotations` is a list of dict with
`image_id`, `fpath`, `im_height`, `im_width` and `category_id`.

Here is an example.
```
{
    'annotations': [
                    {
                        'image_id': 1,
                        'fpath': '/home/BBN/iNat18/images/train_val2018/Plantae/7477/3b60c9486db1d2ee875f11a669fbde4a.jpg',
                        'im_height': 600,
                        'im_width': 800,
                        'category_id': 7477
                    },
                    ...
                   ]
    'num_classes': 8142
}
```
You can use the following code to convert from the original format of iNaturalist. 
The images and annotations can be downloaded at [iNaturalist 2018](https://github.com/visipedia/inat_comp/blob/master/2018/README.md) and [iNaturalist 2017](https://github.com/visipedia/inat_comp/blob/master/2017/README.md)

```bash
# Convert from the original format of iNaturalist
python tools/convert_from_iNat.py --file train2018.json --root /home/iNat18/images --sp /home/BBN/jsons
```


## Citing this repository
If you find this code useful in your research, please consider citing us:
```
@article{zhou2020BBN,
	title={{BBN}: Bilateral-Branch Network with Cumulative Learning for Long-Tailed Visual Recognition},
	author={Boyan Zhou and Quan Cui and Xiu-Shen Wei and Zhao-Min Chen},
	booktitle={CVPR},
	pages={1--8},
	year={2020}
}
```

## Contacts
If you have any questions about our work, please do not hesitate to contact us by emails.

Xiu-Shen Wei: weixs.gm@gmail.com

Boyan Zhou: zhouboyan94@gmail.com

Quan Cui: cui-quan@toki.waseda.jp
